
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 6
#define COCOAPODS_VERSION_PATCH_AFNetworking 0

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 0

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 0

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 0

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 0

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 0

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 6
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 0

// Crashlytics
#define COCOAPODS_POD_AVAILABLE_Crashlytics
#define COCOAPODS_VERSION_MAJOR_Crashlytics 3
#define COCOAPODS_VERSION_MINOR_Crashlytics 4
#define COCOAPODS_VERSION_PATCH_Crashlytics 1

// Fabric
#define COCOAPODS_POD_AVAILABLE_Fabric
#define COCOAPODS_VERSION_MAJOR_Fabric 1
#define COCOAPODS_VERSION_MINOR_Fabric 6
#define COCOAPODS_VERSION_PATCH_Fabric 1

// RKDropdownAlert
#define COCOAPODS_POD_AVAILABLE_RKDropdownAlert
#define COCOAPODS_VERSION_MAJOR_RKDropdownAlert 0
#define COCOAPODS_VERSION_MINOR_RKDropdownAlert 3
#define COCOAPODS_VERSION_PATCH_RKDropdownAlert 0

// XLForm
#define COCOAPODS_POD_AVAILABLE_XLForm
#define COCOAPODS_VERSION_MAJOR_XLForm 3
#define COCOAPODS_VERSION_MINOR_XLForm 0
#define COCOAPODS_VERSION_PATCH_XLForm 2

