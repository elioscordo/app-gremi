//
//  SettingsViewController.m
//  
//
//  Created by elio on 14/10/15.
//
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initForm];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initForm];
    }
    return self;
}

-(void) initForm{
    
    XLFormDescriptor * form;
    form = [XLFormDescriptor formDescriptorWithTitle:@"Ajustes"];
    
    XLFormSectionDescriptor * sectionVersion = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:sectionVersion];
    
    _userRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Puerta" rowType:XLFormRowDescriptorTypeText title:@"Puerta"];
    [_userRow.cellConfigAtConfigure setObject:@"Numero Puerta" forKey:@"textField.placeholder"];
    [sectionVersion addFormRow:_userRow];
    
    
    _refreshsection = [XLFormSectionDescriptor formSectionWithTitle: [self getLastDate]];
    [form addFormSection:_refreshsection];
    
    _saveButton = [XLFormRowDescriptor formRowDescriptorWithTag:@"Actualizar" rowType:XLFormRowDescriptorTypeButton title:@"Actualizar Opciones"];
    _saveButton.action.formSelector = @selector(doLoad:);
    [_refreshsection addFormRow:_saveButton];
    self.form = form;
}

-(void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue
{
    // super implementation must be called
    NSString *value = (NSString *)newValue;
    if ([formRow.tag isEqualToString:@"Puerta"] && ! [value isEqualToString:@""]){
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        [currentDefaults setObject:value forKey:@"gremi_optionts_user"];
    }
}

-(NSString *)getUser{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [currentDefaults objectForKey:@"gremi_optionts_user"];
    if (data != nil){
        return  (NSString *)data;
    }
    return @"";
}

-(NSString *)getLastDate{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [currentDefaults objectForKey:@"gremi_optionts_date"];
    if (data != nil){
        NSDate *date = (NSDate *)data;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        return  [dateFormatter stringFromDate:date];
    }
    return @"";
}

-(void)doLoad:(XLFormRowDescriptor *)sender{
    CarOptions * options = [[CarOptions alloc]init];
    [options loadOptions];
    [_refreshsection setTitle: [self getLastDate]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
