//
//  Car+CoreDataProperties.h
//  gremimotor
//
//  Created by elio on 07/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Car.h"

NS_ASSUME_NONNULL_BEGIN

@interface Car (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *anyo;
@property (nullable, nonatomic, retain) NSString *cambio;
@property (nullable, nonatomic, retain) NSString *carroceria;
@property (nullable, nonatomic, retain) NSString *combustible;
@property (nullable, nonatomic, retain) NSDate *created;
@property (nullable, nonatomic, retain) NSNumber *km;
@property (nullable, nonatomic, retain) NSString *marca;
@property (nullable, nonatomic, retain) NSString *matricula;
@property (nullable, nonatomic, retain) NSString *modelo;
@property (nullable, nonatomic, retain) NSString *municipio;
@property (nullable, nonatomic, retain) NSString *observaciones;
@property (nullable, nonatomic, retain) NSDecimalNumber *precio;
@property (nullable, nonatomic, retain) NSString *provincia;
@property (nullable, nonatomic, retain) NSNumber *reference;
@property (nullable, nonatomic, retain) NSDate *saved;
@property (nullable, nonatomic, retain) NSString *stand;
@property (nullable, nonatomic, retain) NSString *status;
@property (nullable, nonatomic, retain) NSDate *uploaded;
@property (nullable, nonatomic, retain) NSString *version;
@property (nullable, nonatomic, retain) NSString *sid;
@property (nullable, nonatomic, retain) NSString *log;
@property (nullable, nonatomic, retain) NSOrderedSet<Photo *> *photos;

@end

@interface Car (CoreDataGeneratedAccessors)

- (void)insertObject:(Photo *)value inPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPhotosAtIndex:(NSUInteger)idx;
- (void)insertPhotos:(NSArray<Photo *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPhotosAtIndex:(NSUInteger)idx withObject:(Photo *)value;
- (void)replacePhotosAtIndexes:(NSIndexSet *)indexes withPhotos:(NSArray<Photo *> *)values;
- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSOrderedSet<Photo *> *)values;
- (void)removePhotos:(NSOrderedSet<Photo *> *)values;

@end

NS_ASSUME_NONNULL_END
