//
//  Car+CoreDataProperties.m
//  gremimotor
//
//  Created by elio on 07/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Car+CoreDataProperties.h"

@implementation Car (CoreDataProperties)

@dynamic anyo;
@dynamic cambio;
@dynamic carroceria;
@dynamic combustible;
@dynamic created;
@dynamic km;
@dynamic marca;
@dynamic matricula;
@dynamic modelo;
@dynamic municipio;
@dynamic observaciones;
@dynamic precio;
@dynamic provincia;
@dynamic reference;
@dynamic saved;
@dynamic stand;
@dynamic status;
@dynamic uploaded;
@dynamic version;
@dynamic sid;
@dynamic log;
@dynamic photos;

@end
