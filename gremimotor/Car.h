//
//  Car.h
//  gremimotor
//
//  Created by elio on 16/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Photo.h"

NS_ASSUME_NONNULL_BEGIN


@interface Car : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
-(Photo *)getFirstPhoto;
-(NSString *)createdString;
-(NSDictionary *)toDict;

@end

NS_ASSUME_NONNULL_END

#import "Car+CoreDataProperties.h"
