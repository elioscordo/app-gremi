//
//  Car.m
//  gremimotor
//
//  Created by elio on 16/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "Car.h"

@implementation Car

// Insert code here to add functionality to your managed object subclass

-(Photo *)getFirstPhoto{
    
    return [self.photos firstObject];
}

-(NSString *)createdString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd HH:mm:ss"];
    return[dateFormatter stringFromDate:self.created];
}

-(NSString *)createdToUpload{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    return[dateFormatter stringFromDate:self.created];
}


-(NSString*)getValue:(NSString*)value {
    if (value == nil){
        return @"";
    }else{
        return value;
    }
}

-(NSString *)getUser{
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [currentDefaults objectForKey:@"gremi_optionts_user"];
    if (data != nil){
        return  (NSString *)data;
    }
    return @"";
}
-(NSDictionary *) toDict{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setObject:[self getValue:self.marca] forKey:@"marca"];
    [dict setObject: [self getValue:self.modelo]  forKey: @"modelo"];
    [dict setObject:[self getValue:self.version] forKey:@"version"];
    [dict setObject:[self getValue:self.matricula] forKey:@"matricula"];
    [dict setObject:[self getValue:[self.km stringValue]] forKey:@"km" ];
    [dict setObject:[self getValue:[self.anyo stringValue] ] forKey:@"anyo"];
    [dict setObject:[self getValue:self.carroceria] forKey:@"carroceria"];
    [dict setObject:[self getValue:self.cambio] forKey:@"cambio"];
    [dict setObject:[self getValue:self.combustible] forKey:@"combustible"];
    [dict setObject:[self getValue:self.observaciones] forKey:@"observaciones"];
    [dict setObject:[self getValue:self.provincia] forKey:@"provincia"];
    [dict setObject:[self getValue:self.municipio] forKey:@"municipio"];
    [dict setObject:[self getValue:[self.precio stringValue]] forKey:@"precio"];
    [dict setObject:[self getValue:self.stand] forKey:@"stand"];
    [dict setObject:[self getValue:self.sid] forKey:@"id"];
    [dict setObject:[self getValue:self.createdToUpload] forKey:@"fecha_intro"];
    [dict setObject:[self getValue:self.getUser] forKey:@"puerta"];
    
    
    return dict;
}
@end
