//
//  CarOptions.h
//  gremimotor
//
//  Created by elio on 17/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoSaver
-(void)syncPhotos:(NSArray*)images;
@end

NSString static *const STATUS_PENDING = @"Pendiente";
NSString static *const STATUS_CREATED = @"Creado";
NSString static *const STATUS_UPLOADING = @"Procesando";
NSString static *const STATUS_UPLOADED = @"Subido";
NSString static *const STATUS_ERROR = @"Error";


@interface CarOptions : NSObject

@property NSMutableArray *marcaSource;
@property NSMutableArray *emptySource;
@property NSMutableDictionary *modeloSource;
@property NSMutableArray *standSource;
@property NSMutableArray *provinciaSource;
@property NSMutableDictionary *provinciaKeys;

@property NSMutableArray *anyoSource;
@property NSMutableDictionary *municipioSource;
@property NSMutableArray *combustibleSource;
@property NSMutableArray *cambioSource;
@property NSMutableArray *carroceriaSource;
@property NSMutableArray *jsonArray;
-(void)refreshOptions;
-(void)loadOptions;
-(NSArray *)getModelo: (NSString *)marca;
-(NSArray *)getMunicipio: (NSString *)provincia;

@end
