//
//  CarOptions.m
//  gremimotor
//
//  Created by elio on 17/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "CarOptions.h"

@implementation CarOptions

-(NSMutableArray *)getMunicipio: (NSString *)provincia{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    [temp addObject:@""];
    for (NSDictionary *dict  in _municipioSource[provincia]){
        [temp addObject:dict[@"value"]];
    }
    return temp;
}
-(NSMutableArray *)getModelo: (NSString *)marca{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    [temp addObject:@""];
    for (NSDictionary *dict  in _modeloSource[marca]){
        [temp addObject:dict[@"value"]];
    }
    [temp addObject:@"Otros"];
    return temp;
 }

-(void) getYears{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    int actual = [yearString intValue];
    _anyoSource = [[NSMutableArray alloc]init];
    for (int i = actual; i>=1980; i--) {
        [_anyoSource addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

-(void)loadOptions{
    NSError *err;
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url=[NSURL URLWithString:@"http://gremimotor.com/app/ajustes.php"];
    NSURLRequest *req=[NSURLRequest requestWithURL:url];
    NSData *data  = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&err];
    [currentDefaults setObject:data forKey:@"gremi_optionts"];
    [currentDefaults setObject:[NSDate date] forKey:@"gremi_optionts_date"];
}

-(void)refreshOptions{
    _emptySource = [[NSMutableArray alloc]init];
    [_emptySource addObject:@""];
    if (_jsonArray == nil){
        _marcaSource = [[NSMutableArray alloc]init];
        [_marcaSource addObject:@""];
        _provinciaKeys = [[NSMutableDictionary alloc]init];
        _provinciaSource = [[NSMutableArray alloc]init];
        NSError *err;
        NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [currentDefaults objectForKey:@"gremi_optionts"];
        if (data == nil){
            NSURL *url=[NSURL URLWithString:@"http://gremimotor.com/app/ajustes.php"];
            NSURLRequest *req=[NSURLRequest requestWithURL:url];
            data = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&err];
            [currentDefaults setObject:data forKey:@"gremi_optionts"];
            [currentDefaults setObject:[NSDate date] forKey:@"gremi_optionts_date"];
            
        }
        _jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        [self getYears];
        for(NSDictionary *item in _jsonArray)
        {
          //  NSLog(@"%@", item);
            
            if ([item[@"field"] isEqualToString: @"cambio"]) {
                _cambioSource = item[@"values"];
                [_cambioSource insertObject:@"" atIndex:0];
            }
            if ([item[@"field"] isEqualToString: @"stand"]) {
                _standSource = item[@"values"];
                [_standSource insertObject:@"" atIndex:0];
            }
            
            if ([item[@"field"] isEqualToString: @"provincia"]) {
                for (NSDictionary *provincia in item[@"values"]){
                    [_provinciaKeys setObject:provincia[@"key"] forKey:provincia[@"value"]];
                    [_provinciaSource addObject :provincia[@"value"]];
                }
                
            }
            if ([item[@"field"] isEqualToString: @"municipio"]) {
                _municipioSource = [[NSMutableDictionary alloc]init];
                for (NSDictionary *dic in item[@"values"]){
                    NSString *key = dic[@"id_provincia"];
                    NSMutableArray *list  = [_municipioSource objectForKey:key];
                    if (list == nil){
                        list = [[NSMutableArray alloc]init];
                        [list addObject:dic];
                        [_municipioSource setObject:list forKey:key];
                    }
                    else{
                        [list addObject:dic];
                    }
                }
                
            }
            
            if ([item[@"field"] isEqualToString: @"carroceria"]) {
                
                _carroceriaSource = item[@"values"];
                [_carroceriaSource insertObject:@"" atIndex:0];
            }
            if ([item[@"field"] isEqualToString: @"combustible"]) {
                _combustibleSource = item[@"values"];
                [_combustibleSource insertObject:@"" atIndex:0];
            }
            if ([item[@"field"] isEqualToString: @"marca"]) {
                _marcaSource = item[@"values"];
            }
            if ([item[@"field"] isEqualToString: @"stand"]) {
                _standSource = item[@"values"];
                 [_standSource insertObject:@"" atIndex:0];
            }
            
            if ([item[@"field"] isEqualToString: @"modelo"]) {
                _modeloSource = [[NSMutableDictionary alloc]init];
                
                for (NSDictionary *dic in item[@"values"]){
                    NSString *key = dic[@"marca"];
                    NSMutableArray *list  = [_modeloSource objectForKey:key];
                    if (list == nil){
                        list = [[NSMutableArray alloc]init];
                        
                        [list addObject:dic];
                        [_modeloSource setObject:list forKey:key];
                    }
                    else{
                        [list addObject:dic];
                    }
                }
                
            }
        }
        
    }
    
}
@end
