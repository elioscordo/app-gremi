//
//  CarTableViewCell.h
//  gremimotor
//
//  Created by elio on 22/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Car.h"
#import "CarTableViewController.h"

@interface CarTableViewCell : UITableViewCell<UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *created;
@property (weak, nonatomic) IBOutlet UILabel *matricula;
@property (weak, nonatomic) IBOutlet UILabel *marca;
@property (weak, nonatomic) IBOutlet UIButton *genericButton;
@property Car *instance;
@property (nonatomic, assign) id<Uploader> delegate;
- (IBAction)genericAction:(id)sender;

@end
