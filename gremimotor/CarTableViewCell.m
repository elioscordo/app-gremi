//
//  CarTableViewCell.m
//  gremimotor
//
//  Created by elio on 22/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "CarTableViewCell.h"
#import "CarTableViewController.h"
#import "RKDropdownAlert.h"

@implementation CarTableViewCell
@synthesize delegate = _delegate;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)genericAction:(id)sender {
    
    if ([_instance.status isEqualToString:STATUS_CREATED]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quiere subir este vehículo?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Subir", nil];
        [alert show];
        alert.tag = 0;
        
        return;
    }
    if ([_instance.status isEqualToString: STATUS_UPLOADING]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quiere subir las fotos de este vehículo?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Subir", nil];
         alert.tag = 1;
        [alert show];
        [ self.delegate uploadPhoto: _instance];
   
    }
    if ([_instance.status isEqualToString: STATUS_PENDING]){
        [RKDropdownAlert title:@"Coche Pendiente" message:@"La ficha del coche no está completa" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor] time:10];
    }
    if (_instance.status == STATUS_ERROR){
        NSString * msg = @"";
        if (_instance.log != nil){
            msg = _instance.log;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!" message:msg delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Subir", nil];
        [alert show];
        alert.tag = 0;
        return;
    }
        
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1){
        if (alertView.tag == 0){
            [ self.delegate uploadCar: _instance];
        }
        if (alertView.tag == 1){
            [ self.delegate uploadPhoto: _instance];
        }
    }
}

@end
