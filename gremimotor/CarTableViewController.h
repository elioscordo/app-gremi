//
//  CarTableViewController.h
//  gremimotor
//
//  Created by elio on 16/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Car.h"
#import "FormViewController.h"
#import "CarOptions.h"
#import "UploadedDelegate.h"


@interface CarTableViewController : UITableViewController<Uploader,UIAlertViewDelegate>


@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) NSMutableArray *cars;
@property (nonatomic, strong) Car *instance;
@property  NSNumber *redirect;
@property (nonatomic, strong) UIBarButtonItem * uploadButton;


@end
