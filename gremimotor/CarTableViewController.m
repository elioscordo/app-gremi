//
//  CarTableViewController.m
//  gremimotor
//
//  Created by elio on 16/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "CarTableViewController.h"
#import "CarTableViewCell.h"
#import "AFHTTPRequestOperationManager.h"
#import "RKDropdownAlert.h"
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

@interface CarTableViewController ()

@end

@implementation CarTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Añadir"  style:UIBarButtonItemStyleBordered target:self action:@selector(insertNewObject:)];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    UIBarButtonItem *settingButton = [[UIBarButtonItem alloc]
                                     initWithTitle:@"Ajustes"  style:UIBarButtonItemStyleBordered
                                     target:self
                                      action:@selector(doSettings:)];
    
    UIBarButtonItem *eraseButton = [[UIBarButtonItem alloc]
                                      initWithTitle:@"Borrar"  style:UIBarButtonItemStyleBordered
                                      target:self
                                      action:@selector(doDelete:)];
    
    UIBarButtonItem *negativeSeparator = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSeparator.width = 60;
    
    
    _uploadButton = [[UIBarButtonItem alloc]
                                initWithTitle:@"Subir Creados"  style:UIBarButtonItemStyleBordered
                                     target:self
                                     action:@selector(confirmUpload:)];
    
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_1.png"]];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    
    NSArray *arrBtns = [[NSArray alloc]initWithObjects:addButton,negativeSeparator,settingButton, nil];
    NSArray *arr2Btns = [[NSArray alloc]initWithObjects:eraseButton,negativeSeparator,_uploadButton, nil];
    self.editButtonItem.title = NSLocalizedString(@"Borrar", @"Borrar");
    self.navigationItem.rightBarButtonItems = arrBtns;
    self.navigationItem.leftBarButtonItems = arr2Btns;
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([_redirect isEqualToNumber: [NSNumber numberWithBool:YES]]){
        _redirect = [NSNumber numberWithBool:NO];
        [self insertNewObject:nil];
    }else{
        [self fetchTheData];
        [self.tableView reloadData];
    }
}

- (void)fetchTheData
{
    //  create fetch object, this object fetch’s the objects out of the database
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];

    NSArray *fetchedObjects = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects != nil)
    {
        _cars = [[NSMutableArray alloc]initWithArray:fetchedObjects];
    }
    [self countCreated];
    
}
-(void)countCreated{
    int num = 0;
    for (Car *car in  _cars){
        if ([car.status isEqualToString:STATUS_CREATED]){
            num++;
        }
    }
    NSString *car = @"coches";
    if (num == 1){
        car =@"coche";
    }
    NSString *for1 = [NSString stringWithFormat:@"Subir %i %@",num,car ];
    [_uploadButton setTitle:for1];
 
}

-(void)confirmUpload:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quiere subir todos los vehículo creados?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Subir", nil];
    [alert show];
    alert.tag = 0;
}
-(void)doDelete:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Quiere borrar vehículos?" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Borrar Pendientes", @"Borrar subidos", @"Borrar Todos",  nil];
     [alert show];
     alert.tag = 1;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 0){
        if (buttonIndex == 1){
            [self uploadCreated];
        }
    }
    if (alertView.tag == 1){
        NSMutableArray *discardedItems = [NSMutableArray array];
        if (buttonIndex == 1){
            for (Car *car in  _cars){
                if ([car.status isEqualToString:STATUS_PENDING]){
                    [discardedItems addObject:car];
                }
            }
            [_cars removeObjectsInArray:discardedItems];
         
        }
        if (buttonIndex == 2){
            for (Car *car in  _cars){
                if ([car.status isEqualToString:STATUS_UPLOADED]){
                    [discardedItems addObject:car];
                }
            }
            [_cars removeObjectsInArray:discardedItems];
           
        }
        if (buttonIndex == 3){
            for (Car *car in  _cars){
                [discardedItems addObject:car];
            }
            [_cars removeObjectsInArray:discardedItems];
            
        }
        for (Car *car in discardedItems) {
            [_managedObjectContext deleteObject:car];
        }
        NSError *error = nil;
        if (![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self.tableView reloadData];
    }
    
}
-(void)uploadCreated{
    BOOL created = NO;
    for (Car *car in  _cars){
        if ([car.status isEqualToString:STATUS_CREATED]){
                     [self uploadCar:car];
            created = YES;
        }
    }
    if (created){
        [RKDropdownAlert title:@"Subiendo Coches" message:@"" backgroundColor:[UIColor greenColor] textColor:[UIColor whiteColor] time:4];

    }else{
        [RKDropdownAlert title:@"Error" message:@"No hay coches creados" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor] time:4];
    }
}

-(void)doSettings:(id)sender{
    [self performSegueWithIdentifier: @"SettingsSegue" sender: self];
}

- (void)insertNewObject:(id)sender {
    [self performSegueWithIdentifier: @"DetailSegue" sender: self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cars count];
}
- (void)viewDidAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
}
// foto
-(void)uploadPhoto:(Car *)car{
    if (car == nil || [car.photos count] == 0){
        return;
    }
    Photo *photo =car.photos[0];
    UIImage *image = photo.image;
    NSLog(@"size to upload %f,%f"  , image.size.height, image.size.width);
    NSData *imageData = UIImageJPEGRepresentation(image, 0.9);
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://server.url"]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *parameters = @{@"id": car.sid};
    AFHTTPRequestOperation *op = [manager POST:@"http://gremimotor.com/app/upload_foto_feria.php" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageData name:@"foto" fileName:@"foto.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *e = nil;
        NSString *stringData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSData *data = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData: data
                              options: NSJSONReadingMutableContainers
                              error: &e];
        if (json[@"status"]){
            car.status = STATUS_UPLOADED;
        }else{
            car.log = stringData;
            car.status = STATUS_ERROR;
        }
        NSError *error = nil;
        if (![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self.tableView reloadRowsAtIndexPaths:[self.tableView  indexPathsForVisibleRows]
                              withRowAnimation:UITableViewRowAnimationNone];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        car.status = STATUS_ERROR;
        car.log =  operation.responseString;
        if (![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self.tableView reloadRowsAtIndexPaths:[self.tableView  indexPathsForVisibleRows]
                              withRowAnimation:UITableViewRowAnimationNone];
        
    }];
    [op start];
}

-(void)uploadCar:(Car *)car{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *parameters = [car toDict];
    __block CarTableViewController *blocksafeSelf = self;
    
    [manager POST:@"http://gremimotor.com/app/upload_coche_feria.php" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *stringData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSError *e = nil;
        NSData *data = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization
                              JSONObjectWithData: data
                              options: NSJSONReadingMutableContainers
                              error: &e];
        if (json[@"id"]){
            car.status = STATUS_UPLOADING;
            car.sid = [json[@"id"] stringValue];
            [blocksafeSelf uploadPhoto:car];
        }else{
            car.log =stringData;
            car.status = STATUS_ERROR;
        }
        NSError *error = nil;
        if (![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self.tableView reloadRowsAtIndexPaths:[self.tableView  indexPathsForVisibleRows]
                              withRowAnimation:UITableViewRowAnimationNone];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        car.status = STATUS_ERROR;
        car.log =  operation.responseString;
        if (![_managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        [self.tableView reloadRowsAtIndexPaths:[self.tableView  indexPathsForVisibleRows]
                              withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CarTableViewCell"];
    if (cell == nil){
        NSArray *parts = [[NSBundle mainBundle] loadNibNamed:@"CarTableViewCell" owner:nil options:nil];
        cell = [parts objectAtIndex:0];
    }
    cell.instance =  [_cars objectAtIndex:indexPath.row];
 
    if (cell.instance.matricula != nil){
        NSLog(@"%@",cell.instance.matricula);
        cell.matricula.text = cell.instance.matricula;
    }
    
    if (cell.instance.marca != nil){
        cell.marca.text = cell.instance.marca;
    }else{
        cell.marca.text = @"";
    }
    if ([cell.instance.status isEqualToString: STATUS_UPLOADING]){
        [cell.genericButton setTitle: @"Subiendo"  forState:UIControlStateNormal];
        cell.status.textColor = UIColorFromRGB(0xCCCCCC);
        
        cell.genericButton.hidden = NO;
    }
    if ([cell.instance.status isEqualToString: STATUS_ERROR]){
        [cell.genericButton setTitle: @"Error"  forState:UIControlStateNormal];
        cell.genericButton.hidden = NO;
        cell.status.textColor = UIColorFromRGB(0xFF0000);
    }
    
    if ([cell.instance.status isEqualToString: STATUS_PENDING]){
        cell.genericButton.hidden = YES;
        cell.status.textColor = UIColorFromRGB(0xFF9E3E);
    }
    if([cell.instance.status isEqualToString:STATUS_UPLOADED]){
        cell.genericButton.hidden = YES;
        cell.status.textColor = UIColorFromRGB(0x27ae60);
    }
    if ([cell.instance.status isEqualToString: STATUS_CREATED]){
        [cell.genericButton setTitle: @"SUBIR"  forState:UIControlStateNormal];
        cell.genericButton.hidden = NO;
        cell.status.textColor = UIColorFromRGB(0x2e8ece);
    }
    
    cell.status.text = cell.instance.status;
    cell.created.text = [cell.instance createdString];
    cell.image.contentMode = UIViewContentModeScaleAspectFit;
    cell.image.image = [cell.instance getFirstPhoto].image ;
    cell.delegate = self;
  
    return cell;
}



// Override to support conditional editing of the table view.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the specified item to be editable.
//    return YES;





//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        // Delete the row from the data source
//        [_cars removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
//        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
//    }   
//}
//
//
//// Override to support rearranging the table view.
//- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
//}
//
//// Override to support conditional rearranging of the table view.
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return NO if you do not want the item to be re-orderable.
//    return YES;
//}

#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSString *segueIdentifier = [segue identifier];
    if ([segueIdentifier isEqualToString:@"DetailSegue"]) // This can be defined via Interface Builder
    {
        
        FormViewController *controller = [segue destinationViewController];
        if ([sender  isKindOfClass:[CarTableViewCell class]]){
            controller.instance = ((CarTableViewCell *)sender).instance;
        }
        controller.managedObjectContext = self.managedObjectContext;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CarTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"DetailSegue" sender:cell];
}

-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
    if ([segue.identifier isEqualToString:@"UnwindToNew"]) {
        _redirect = [NSNumber numberWithBool:YES];
    }
    if ([segue.identifier isEqualToString:@"UnwindToUpload"]) {
        _redirect = [NSNumber numberWithBool:YES];
        FormViewController *last = [segue sourceViewController];
        [self uploadCar:last.instance];
    }

}
- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    // Make sure you call super first
    [super setEditing:editing animated:animated];
    
    if (editing)
    {
        self.editButtonItem.title = NSLocalizedString(@"Hecho", @"Hecho");
    }
    else
    {
        self.editButtonItem.title = NSLocalizedString(@"Borrar", @"Borrar");
    }
}
@end
