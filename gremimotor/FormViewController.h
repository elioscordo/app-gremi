//
//  FormViewController.h
//  gremimotor
//
//  Created by elio on 17/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLForm.h" 
#import "CarOptions.h"



#import <CoreData/CoreData.h>
#import "Car.h"
#import "Photo.h"
#import "UppercaseCell.h"
#import "YearCell.h"

#import "RPMultipleImagePickerViewController.h"



@interface FormViewController : XLFormViewController<UIImagePickerControllerDelegate,PhotoSaver>

@property CarOptions *options;
@property Car *instance;
@property NSDictionary *errors;
@property (nonatomic, strong) UIImagePickerController *pickerController;

@property (nonatomic, strong) RPMultipleImagePickerViewController *multipleImagePicker;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property XLFormRowDescriptor *marcaRow;
@property XLFormRowDescriptor *modeloRow;
@property XLFormRowDescriptor *versionRow;


@property XLFormRowDescriptor *kmRow;
@property XLFormRowDescriptor *anyoRow;
@property XLFormRowDescriptor *matriculaRow;

@property XLFormRowDescriptor *carroceriaRow;
@property XLFormRowDescriptor *cambioRow;
@property XLFormRowDescriptor *combustibleRow;


@property XLFormRowDescriptor *observacionesRow;

@property XLFormRowDescriptor *municipioRow;
@property XLFormRowDescriptor *provinciaRow;
@property XLFormRowDescriptor *standRow;
@property XLFormRowDescriptor *precioRow;


@property XLFormRowDescriptor *saveButton;
@property XLFormRowDescriptor *saveNextButton;
@property XLFormRowDescriptor *saveUploadButton;


@property  XLFormSectionDescriptor *sectionButtons;

-(void)doSave:(XLFormRowDescriptor *)sender;
-(void)doFollow:(XLFormRowDescriptor *)sender;
-(void)syncPhotos:(NSArray*)images;



-(BOOL)loadPhotos;

@end