//
//  FormViewController.m
//  gremimotor
//
//  Created by elio on 17/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "FormViewController.h"
#import "RKDropdownAlert.h"
#import "MaxCharValidator.h"


@interface FormViewController() <XLFormDescriptorDelegate, UIImagePickerControllerDelegate , UINavigationControllerDelegate>
@end



NSString *const kMarca = @"marca";
NSString *const kProvincia = @"provincia";



@implementation FormViewController

-(void) createInstance{
    
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Car" inManagedObjectContext:_managedObjectContext];
    _instance = (Car *)[NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:_managedObjectContext];
    _instance.created = [NSDate date];
    _instance.status = STATUS_PENDING;
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    // Save the context.
    NSError *error = nil;
    if (![_managedObjectContext save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

}

-(BOOL)validate{
    NSMutableDictionary *errors = [[NSMutableDictionary alloc]init];
    if (_marcaRow.value == nil || [_marcaRow.value  isEqual: @""]){
        [errors setObject:@"Debe seleccionar una marca" forKey:@"marca"];
    
    }
    if (_matriculaRow.value == nil || [_matriculaRow.value  isEqual: @""]){
        [errors setObject:@"Debe seleccionar una matricula" forKey:@"matricula"];
        
    }
    if (_standRow.value == nil || [_standRow.value  isEqual: @""]){
        [errors setObject:@"Debe seleccionar un stand" forKey:@"stand"];
    }
    if (_anyoRow.value == nil || _anyoRow.value == 0 ){
        [errors setObject:@"Debe seleccionar una año" forKey:@"stand"];
    }
    if (_kmRow.value == nil || _kmRow.value == 0 ){
        [errors setObject:@"Debe seleccionar el kilometraje" forKey:@"stand"];
    }
    _errors = errors;
    return [errors count] == 0;
}

- (void)viewDidAppear:(BOOL)animated
{
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
}
-(void)save : (XLFormRowDescriptor *)sender{
    [self deselectFormRow:sender];
    NSLog(@"save");
    if (_instance == nil){
        [self createInstance];
    }
    [self saveInstance];
}

-(void)doSave:(XLFormRowDescriptor *)sender
{
    if ([self validate]){
        [self save:sender];
        [self performSegueWithIdentifier:@"UnwindToList" sender:self];
    }else{
         [self showErrors];
    }
}

-(void)showErrors{
    NSString * msg = @"";
    for (NSString* key in _errors) {
        msg = [NSString stringWithFormat:@"%@ \n %@", _errors[key],msg];
    }
    [RKDropdownAlert title:@"Hay Errores" message:msg backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor] time:10];
}

-(void)doFollow:(XLFormRowDescriptor *)sender
{
    if ([self validate]){
        [self save:sender];
        [self performSegueWithIdentifier:@"UnwindToNew" sender:self];
    }else{
        [self showErrors];
    }
}
-(void)doUpload:(XLFormRowDescriptor *)sender
{
    if ([self validate]){
        [self save:sender];
        
        [self performSegueWithIdentifier:@"UnwindToUpload" sender:self];
    }else{
        [self showErrors];
    }
}


-(void) initForm{
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[UppercaseCell class] forKey:UppercaseCellSelector];
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[YearCell class] forKey:YearCellSelector];
    
    _options = [[CarOptions alloc]init];
    [_options refreshOptions];
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"Coche"];
    
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Vehiculo"];
    //  section = [XLFormSectionDescriptor formSectionWithTitle:@"Inline Selectors"];
    
    [form addFormSection:section];
   
    // marca
    _marcaRow = [XLFormRowDescriptor formRowDescriptorWithTag:kMarca rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Marca"];
    _marcaRow.selectorOptions = _options.marcaSource;
    _marcaRow.value = @"";
    
    [section addFormRow:_marcaRow];
    
    
    // modelo
    _modeloRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Modelo" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Modelo"];
    _modeloRow.value = @"";
     NSMutableArray *empty = _options.emptySource;
    _modeloRow.selectorOptions = empty;
    [_modeloRow setHidden:[NSNumber numberWithBool:YES]];
    [section addFormRow:_modeloRow];
    
    
    
    XLFormSectionDescriptor * sectionVersion = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:sectionVersion];
    _versionRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Version" rowType:XLFormRowDescriptorTypeText title:@"Versión"];
    [_versionRow.cellConfigAtConfigure setObject:@"Versión" forKey:@"textField.placeholder"];
   // [_versionRow.cellConfigAtConfigure setObject:[UIColor redColor] forKey:@"textField.textLabel.textColor"];
    [_versionRow.cellConfig setObject:[UIColor grayColor] forKey:@"textField.textColor"];
 //   [_versionRow.cellConfigAtConfigure setObject:@(NSTextAlignmentCenter) forKey:@"textField.textAlignment"];
   [_versionRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    [sectionVersion addFormRow:_versionRow];

    
    XLFormSectionDescriptor * section2 = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:section2];
    
    _matriculaRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Version" rowType:UppercaseCellSelector title:@"Matricula"];
    [_matriculaRow.cellConfigAtConfigure setObject:@"Matricula" forKey:@"textField.placeholder"];
    [_matriculaRow.cellConfig setObject:[UIColor grayColor] forKey:@"textField.textColor"];
    [_matriculaRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    [section2 addFormRow:_matriculaRow];
    _kmRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Km" rowType:XLFormRowDescriptorTypeNumber title:@"Km"];
    [_kmRow.cellConfigAtConfigure setObject:@"Km" forKey:@"textField.placeholder"];
    [_kmRow.cellConfig setObject:[UIColor grayColor] forKey:@"textField.textColor"];
    [_kmRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    [section2 addFormRow:_kmRow];
    
    
      _anyoRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Anyo" rowType:YearCellSelector title:@"Año"];
    [_anyoRow.cellConfigAtConfigure setObject:@"Año" forKey:@"textField.placeholder"];
    [_anyoRow.cellConfig setObject:[UIColor grayColor] forKey:@"textField.textColor"];
    [_anyoRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    [section2 addFormRow:_anyoRow];

    
    XLFormSectionDescriptor * sectionFeatures = [XLFormSectionDescriptor formSectionWithTitle:@"Característica"];
    [form addFormSection:sectionFeatures];
   
    _carroceriaRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Carroceria" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Carroceria"];
     _carroceriaRow.selectorOptions = _options.carroceriaSource;
    [sectionFeatures addFormRow:_carroceriaRow];
    
    _cambioRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Cambio" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Cambio"];
    _cambioRow.selectorOptions = _options.cambioSource;
    [sectionFeatures addFormRow:_cambioRow];
    
    
    _combustibleRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Combustible" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Combustible"];
    
    //[_combustibleRow.cellConfigAtConfigure setObject:@"Combustible" forKey:@"textField.placeholder"];
    _combustibleRow.selectorOptions = _options.combustibleSource;
    [sectionFeatures addFormRow:_combustibleRow];
    
    
    XLFormSectionDescriptor * sectionObservaciones = [XLFormSectionDescriptor formSectionWithTitle:@"Observaciones"];
    [form addFormSection:sectionObservaciones];
    
    _observacionesRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Observaciones" rowType:XLFormRowDescriptorTypeTextView title:@"Observaciones"];
    [_observacionesRow.cellConfigAtConfigure setObject:@"Observaciones" forKey:@"textView.placeholder"];
    [_observacionesRow.cellConfig setObject:[UIColor grayColor] forKey:@"textView.textColor"];
   // [_observacionesRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    
    
    [sectionObservaciones addFormRow:_observacionesRow];
    
    XLFormSectionDescriptor * sectionLocation = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:sectionLocation];
    
    
    _provinciaRow = [XLFormRowDescriptor formRowDescriptorWithTag:kProvincia rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Provincia"];
    _provinciaRow.selectorOptions = _options.provinciaSource;
    [sectionLocation addFormRow:_provinciaRow];
    
    _municipioRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Municipio" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Municipio"];
    _municipioRow.selectorOptions = _options.provinciaSource;
    [sectionLocation addFormRow:_municipioRow];
    
    
    XLFormSectionDescriptor * sectionFinal = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:sectionFinal];
    
    
    
    _precioRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Precio" rowType:XLFormRowDescriptorTypeNumber title:@"Precio"];
    [_precioRow.cellConfigAtConfigure setObject:@"Consultar en feria" forKey:@"textField.placeholder"];
    [_precioRow.cellConfig setObject:[UIColor grayColor] forKey:@"textField.textColor"];
    [_precioRow.cellConfigAtConfigure setObject:[NSNumber numberWithFloat:0.8] forKey:XLFormTextFieldLengthPercentage];
    
    [sectionFinal addFormRow:_precioRow];
    
    
    
    _standRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"Stand" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Stand"];
    _standRow.selectorOptions = _options.standSource;
    [sectionFinal addFormRow:_standRow];
    
    
    _sectionButtons = [XLFormSectionDescriptor formSectionWithTitle:@""];
    [form addFormSection:_sectionButtons];
    
    _saveButton = [XLFormRowDescriptor formRowDescriptorWithTag:@"Save" rowType:XLFormRowDescriptorTypeButton title:@"AÑADIR Y SALIR"];
    _saveButton.action.formSelector = @selector(doSave:);
    [_sectionButtons addFormRow:_saveButton];
    
    
    _saveUploadButton = [XLFormRowDescriptor formRowDescriptorWithTag:@"Save" rowType:XLFormRowDescriptorTypeButton title:@"SUBIR Y CONTINUAR"];
    _saveUploadButton.action.formSelector = @selector(doUpload:);
    
    [_sectionButtons addFormRow:_saveUploadButton];
    
    _saveNextButton = [XLFormRowDescriptor formRowDescriptorWithTag:@"Save" rowType:XLFormRowDescriptorTypeButton title:@"AÑADIR Y CONTINUAR"];
    _saveNextButton.action.formSelector = @selector(doFollow:);
    
    [_sectionButtons addFormRow:_saveNextButton];
    
   
    [sectionFinal addFormRow:_standRow];
    
    
    self.form = form;
}

-(void)saveInstance{
    _instance.marca  = _marcaRow.value;
    _instance.modelo = _modeloRow.value;
    _instance.version = _versionRow.value;
    _instance.matricula = _matriculaRow.value;
    _instance.km = _kmRow.value;
    _instance.anyo = _anyoRow.value;
    _instance.carroceria = _carroceriaRow.value;
    _instance.cambio = _cambioRow.value;
    _instance.combustible = _combustibleRow.value;
    _instance.observaciones = _observacionesRow.value;
    _instance.provincia = _provinciaRow.value;
    _instance.municipio = _municipioRow.value;
    _instance.precio = _precioRow.value;
    _instance.stand = _standRow.value;
    _instance.status = STATUS_CREATED;
    NSError *error = nil;
    if (![_managedObjectContext save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}


-(BOOL)checkInt:(id)value{
    NSNumber *n = (NSNumber *)(value);
    if (n != nil && ![n isEqualToNumber:[NSNumber numberWithInt:0]]){
        return YES;
    }
    return NO;

}
-(void)loadInstance{
    if (_instance.marca != nil){
        _marcaRow.value =_instance.marca;
        _modeloRow.selectorOptions = [_options getModelo:_marcaRow.value];
        [self reloadFormRow:_marcaRow];
    }
    if (_instance.modelo != nil){
        [_modeloRow setHidden:[NSNumber numberWithBool:NO]];
        _modeloRow.value =_instance.modelo;
        [self reloadFormRow:_modeloRow];
        
    }
    if (_instance.version != nil){
        _versionRow.value =_instance.version;
         [self reloadFormRow:_versionRow];
    }
    
    if (_instance.matricula != nil){
        _matriculaRow.value =_instance.matricula;
         [self reloadFormRow:_matriculaRow];
    }
    
    
    if ([self checkInt:_instance.km]){
        _kmRow.value =_instance.km;
        [self reloadFormRow:_kmRow];
    }
    if ([self checkInt:_instance.anyo]){
        _anyoRow.value =_instance.anyo;
        [self reloadFormRow:_anyoRow];

    }
    if (_instance.carroceria != nil){
        _carroceriaRow.value =_instance.carroceria;
        [self reloadFormRow:_carroceriaRow];
    }
    if (_instance.cambio != nil){
        _cambioRow.value =_instance.cambio;
        [self reloadFormRow:_cambioRow];
    }
    if (_instance.combustible != nil){
        _combustibleRow.value =_instance.combustible;
        [self reloadFormRow:_combustibleRow];
    }
    if (_instance.observaciones != nil){
        _observacionesRow.value =_instance.observaciones;
        [self reloadFormRow:_observacionesRow];
    }
    if (_instance.provincia != nil){
        _provinciaRow.value =_instance.provincia;
        _municipioRow.selectorOptions = [_options getMunicipio:_instance.provincia];
        [self reloadFormRow:_provinciaRow];
    }
    if (_instance.municipio != nil){
        _municipioRow.value =_instance.municipio;
        [self reloadFormRow:_municipioRow];
    }
    if ([self checkInt:_instance.precio] ){
        _precioRow.value =_instance.precio;
        [self reloadFormRow:_precioRow];
    }
    if (_instance.stand != nil){
        _standRow.value =_instance.stand;
        [self reloadFormRow:_precioRow];
    }
    if ([_instance.status isEqualToString:STATUS_UPLOADED ]){
        [_sectionButtons setHidden:[NSNumber numberWithBool:YES]];
        self.form.disabled = YES;
    }else{
        [_sectionButtons setHidden:[NSNumber numberWithBool:NO]];
        self.form.disabled = NO;
    }
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initForm];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initForm];
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    if (_instance != nil){
        [self loadInstance];
    }else{
        [self takePhoto:nil];
    }
    
    UIBarButtonItem * photoButton = [[UIBarButtonItem alloc] initWithTitle:@"Hacer Fotos" style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(takePhoto:)];
    self.navigationItem.rightBarButtonItem = photoButton;
   
}

-(void)takePhoto:(UIBarButtonItem *)button
{

//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    [self presentViewController:picker animated:YES completion:NULL];
    self.multipleImagePicker = [[RPMultipleImagePickerViewController alloc] init];
    self.multipleImagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.multipleImagePicker.delegate = self;
    if ([self loadPhotos]){
        self.multipleImagePicker.editMode =  [NSNumber numberWithBool:1];
        [self.navigationController pushViewController:self.multipleImagePicker animated:YES];
    }else{
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        self.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController: self.pickerController animated:YES completion:nil];
  
    }
    
}

-(BOOL)loadPhotos{
    if (_instance.photos.count > 0 && self.multipleImagePicker != nil){
        for (Photo* photo in _instance.photos){
            [self.multipleImagePicker addImage:photo.image];
        }
        return YES;
    }
    return NO;
}
//
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.multipleImagePicker imagePickerController:picker didFinishPickingMediaWithInfo:info];
    [self.navigationController pushViewController:self.multipleImagePicker animated:YES];
    
}

-(void)deletePhotos{
    if (_instance == nil){
        [self createInstance];
    }
    for (Photo* photo in _instance.photos){
        [_instance removePhotosObject:photo];
    }
}

-(void)syncPhotos:(NSArray *)images{
    if (_instance == nil){
        [self createInstance];
    }
    [self deletePhotos];
    for (UIImage *selected in images){
        NSManagedObjectContext *context = _instance.managedObjectContext;
        Photo *photo = [NSEntityDescription insertNewObjectForEntityForName:@"Photo" inManagedObjectContext:context];
        photo.image = selected;
        NSLog(@"resize %f,%f"  , selected.size.height, selected.size.width);
        [_instance addPhotosObject:photo];
    }
    
    NSError *error = nil;
    if (![_instance.managedObjectContext save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}


-(void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue
{
    // super implementation must be called
    [super formRowDescriptorValueHasChanged:formRow oldValue:oldValue newValue:newValue];
    if ([formRow.tag isEqualToString:kMarca]){
        NSArray *modeloSource = [_options getModelo:newValue];
        _modeloRow.selectorOptions = modeloSource;
        [_modeloRow setHidden:[NSNumber numberWithBool:NO]];
        [self reloadFormRow:_modeloRow];
    }
    if ([formRow.tag isEqualToString:kProvincia]){
        NSString *key =_options.provinciaKeys[newValue] ;
        _municipioRow.selectorOptions = [_options getMunicipio:key];
        [self reloadFormRow:_municipioRow];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
@end
