//
//  Photo+CoreDataProperties.h
//  gremimotor
//
//  Created by elio on 07/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Photo.h"

NS_ASSUME_NONNULL_BEGIN

@interface Photo (CoreDataProperties)

@property (nullable, nonatomic, retain) id image;

@end

NS_ASSUME_NONNULL_END
