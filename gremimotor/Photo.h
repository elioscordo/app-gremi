//
//  Photo.h
//  gremimotor
//
//  Created by elio on 22/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Car;

NS_ASSUME_NONNULL_BEGIN

@interface Photo : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Photo+CoreDataProperties.h"
