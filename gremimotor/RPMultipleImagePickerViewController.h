//
//  MultipleImagePickerViewController.h
//
//  Created by Renato Peterman on 17/08/14.
//  Copyright (c) 2014 Renato Peterman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "RPImageCell.h"
#import "CarOptions.h"

typedef void (^ RPMultipleImagePickerDoneCallback)(NSArray *images);

@interface RPMultipleImagePickerViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UINavigationBarDelegate>
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;

- (IBAction)doneTouch:(id)sender;

@property (nonatomic, copy) RPMultipleImagePickerDoneCallback doneCallback;
@property (nonatomic, strong) UIImagePickerController *pickerController;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) IBOutlet UIButton *btRemover;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIView *bgView;
@property (nonatomic, readwrite) NSUInteger selectedIndex;
@property (nonatomic, readwrite) UIImagePickerControllerSourceType sourceType;
@property (nonatomic, readwrite) NSNumber *editMode;
@property (nonatomic, readwrite) NSNumber *picketOnStart;

- (void)addImage:(UIImage*)image;
- (IBAction)remove:(id)sender;
@property (nonatomic, assign) id<PhotoSaver> delegate;
-(UIImage *)squareCropImageToSideLength:(UIImage *)sourceImage  : (CGFloat ) sideLength;
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;

@end
