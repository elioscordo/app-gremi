//
//  SettingsViewController.h
//  
//
//  Created by elio on 14/10/15.
//
//

#import <UIKit/UIKit.h>
#import "XLForm.h" 
#import "CarOptions.h"
@interface SettingsViewController : XLFormViewController

@property XLFormRowDescriptor *userRow;
@property XLFormRowDescriptor *saveButton;
@property XLFormSectionDescriptor *refreshsection;
@property XLFormRowDescriptor *label;

@end
