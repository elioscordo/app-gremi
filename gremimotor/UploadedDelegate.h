//
//  UploadedDelegate.h
//  gremimotor
//
//  Created by elio on 27/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "AFHTTPRequestOperationManager.h"
#import "CarOptions.h"

@protocol Uploader
-(void)uploadCar:(Car *)car;
-(void)uploadPhoto:(Car *)car;

@end

@interface UploadedDelegate : NSObject<Uploader>

@end
