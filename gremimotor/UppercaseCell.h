//
//  UppercaseCell.h
//  gremimotor
//
//  Created by elio on 09/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "XLFormTextFieldCell.h"
NSString static  *const UppercaseCellSelector = @"selectorTextUpperCase";

@interface UppercaseCell : XLFormTextFieldCell

@end
