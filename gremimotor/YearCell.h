//
//  YearCell.h
//  gremimotor
//
//  Created by elio on 26/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "XLFormTextFieldCell.h"
NSString static  *const YearCellSelector = @"selectorYear";

@interface YearCell : XLFormTextFieldCell

@end
