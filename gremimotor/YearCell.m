//
//  YearCell.m
//  gremimotor
//
//  Created by elio on 26/10/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import "YearCell.h"

@implementation YearCell

- (void)update
{
    self.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    [super update];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 4;
    return [self.formViewController textField:textField shouldChangeCharactersInRange:range replacementString:string];
}
- (void)textFieldDidChange:(UITextField *)textField{
    if([self.textField.text length] > 0) {
            self.rowDescriptor.value = @([self.textField.text integerValue]);
    } else {
        self.rowDescriptor.value = nil;
    }
}

@end
