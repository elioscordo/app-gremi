//
//  main.m
//  gremimotor
//
//  Created by elio on 16/09/15.
//  Copyright © 2015 gremimotor. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
